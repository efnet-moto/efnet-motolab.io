#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'tedski'
SITENAME = u'efnet #motorcycles'
SITEURL = 'http://localhost:8000'

OUTPUT_PATH = 'public/'
PATH = 'content'

TIMEZONE = 'UTC'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

MENUITEMS = (('stats', 'http://stats.efnetmoto.com'), )
DISPLAY_PAGES_ON_MENU = True

# Blogroll
LINKS = (('EFNet', 'http://efnet.org/'),)

# Social widget
SOCIAL = (('gitlab', 'https://gitlab.com/efnet-moto'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
